package com.investree.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "transaksi")
public class Transaksi implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="id_peminjam")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_peminjam;

    @Column(name="id_meminjam")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_meminjam;

    @Column(name = "tenor", length = 10)
    private int tenor;

    @Column(name = "total_pinjaman", length = 50)
    private double total_pinjaman;

    @Column(name = "bunga_persen", length = 5)
    private double bunga_persen;

    @Column(name = "status", nullable = false, length = 45)
    private String status;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_peminjam", referencedColumnName = "id")
    Users users;

}
