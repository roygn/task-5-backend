package com.investree.demo.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "paymentHistory")
public class PaymentHistory implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="id_transaksi")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_transaksi;

    @Column(name = "pembayaran_ke", length = 10)
    private int pembayaran_ke;

    @Column(name = "jumlah", length = 50)
    private double jumlah;

    @Column(name = "bukti_pembayaran", nullable = false)
    private String bukti_pembayaran;
}
