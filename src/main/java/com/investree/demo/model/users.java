package com.investree.demo.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "users")
public class Users implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Column(name = "password",  length = 20)
    private String password;

    @Column(name = "is_active")
    private Boolean is_active;

    @JsonIgnore
    @OneToOne(mappedBy = "users")
    private Users detailUser;

    @OneToMany(mappedBy = "transaksi")
    private Users transaksi;

}
