package com.investree.demo.repository;

import com.investree.demo.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<Users, Long> {
    @Query("select c from Users c")
    public List<Users> getList();
    @Query("select c from Users c WHERE c.id = :id")
    public Users getbyID(@Param("id") Long id);
}
