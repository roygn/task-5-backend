package com.investree.demo.view.impl;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepository;
import com.investree.demo.view.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.*;


public class TransaksiPaymentImple implements TransaksiService {
    @Autowired
    public TransaksiRepository transaksiRepository;

    @Override
    public Map save(Transaksi obj, Long id_peminjam) {
        Map map = new HashMap();
        try {
            Transaksi save = transaksiRepository.save(obj);
            map.put("data", save);
            map.put("code", "200");
            map.put("status", "sukses");
            return map;
        } catch (Exception e) {
            map.put("code", "500");
            map.put("status", "failed");
            return map;
        }

    }

    @Override
    public Map updateStatus(Transaksi obj) {
        Map map = new HashMap();
        try {
            Transaksi update = transaksiRepository.getbyID(obj.getId());
            update.setStatus(obj.getStatus());
            Transaksi doSave = transaksiRepository.save(update);
            map.put("data", doSave);
            map.put("code", "200");
            map.put("status", "sukses");
        } catch (Exception e) {
            map.put("code", "500");
            map.put("status", "failed");
            return map;
        }
        return map;

    }

    @Override
    public Map getAll(Transaksi obj) {
        List<Transaksi> list = new ArrayList<Transaksi>();
        Map map = new HashMap();
        try {

            TransaksiRepository repo = null;
            list = repo.getList();
            map.put("data", list);
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        return null;
    }

}
