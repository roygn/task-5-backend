package com.investree.demo.controller;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepository;
import com.investree.demo.repository.PaymentHistoryRepository;
import com.investree.demo.repository.UserRepository;
import com.investree.demo.view.TransaksiService;
import com.investree.demo.view.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/v1/transaksi")
public class TransaksiController {

    @Autowired
    public TransaksiRepository repo;

    @Autowired
    public UserRepository repoUser;

    @Autowired
    public PaymentHistoryRepository repoPaymentHist;

    @Autowired
    TransaksiService servis;

    @GetMapping("/listPage")
    @ResponseBody
    public ResponseEntity<Map>  getList() {
        Map c = servis.getAll();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @PostMapping("/save/{id_peminjam}")
    public ResponseEntity<Map> save(@PathVariable(value = "id_peminjam") Long id_peminjam, @RequestBody Transaksi objModel) {
        Map map = new HashMap();
        Map obj = servis.save(objModel, id_peminjam);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

}
